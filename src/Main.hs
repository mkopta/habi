module Main where

import System.Environment(getArgs, getProgName)
import Habi.Data
import Habi.InputParser
import Habi.Runner(run)

usage :: String -> String
usage prgname = "Usage: "++ prgname ++" <bf_source>"

checkArgs args prgname | length args == 1 = return ()
                       | otherwise        = error (usage prgname)

main :: IO()
main = do
  args <- getArgs
  prgname <- getProgName
  checkArgs args prgname
  program <- parseFile $ head args
  run program

parseFile :: String -> IO Program
parseFile fileName = do
  source <- readFile fileName
  case parseSource fileName source of
    Left err -> do
      putStr "Parse error at "
      print err
      error "Error while parsing source"
    Right program -> return program
