module Habi.Runner where

import Control.Monad(foldM)
import Data.Char(ord, chr)
import Habi.Data

run :: Program -> IO ()
run p = do
  executeBlocks p newTape
  return ()
  where
    newTape = (0, [0])

executeCycle :: [Block] -> TapeDevice -> IO TapeDevice
executeCycle bs td@(pos, tape)
  | tape!!pos == 0 = return td
  | otherwise      = executeBlocks bs td >>= executeCycle bs

executeBlocks :: [Block] -> TapeDevice -> IO TapeDevice
executeBlocks bs td = foldM (flip executeBlock) td bs

executeBlock :: Block -> TapeDevice -> IO TapeDevice
executeBlock (Instruction instr) = executeInstruction instr
executeBlock (Cycle cycle) = executeCycle cycle

executeInstruction :: Instruction -> TapeDevice -> IO TapeDevice
executeInstruction '<' = executeMoveLeft
executeInstruction '>' = executeMoveRight
executeInstruction '+' = executeIncDec (+1)
executeInstruction '-' = executeIncDec (flip (-) 1)
executeInstruction '.' = executeRead
executeInstruction ',' = executeWrite
executeInstruction _   = error "Ivalid instruction"

executeMoveLeft :: TapeDevice -> IO TapeDevice
executeMoveLeft (position, tape) = if prevPosition < 0
  then error "Pointer out of memory (underrun)"
  else return (prevPosition, tape)
  where prevPosition = position - 1

executeMoveRight :: TapeDevice -> IO TapeDevice
executeMoveRight (position, tape) = if nextPosition < length tape
  then return (nextPosition, tape)
  else return (nextPosition, extendedTape)
  where
    nextPosition = position + 1
    extendedTape = tape ++ [0]

executeIncDec :: (TapePosition -> TapePosition) -> TapeDevice -> IO TapeDevice
executeIncDec f (position, oldTape) = return (position, newTape)
  where newTape = take position oldTape
                  ++ [f (oldTape!!position)]
                  ++ drop (position+1) oldTape

executeRead :: TapeDevice -> IO TapeDevice
executeRead td@(position, tape) = do
  putChar $ chr cell
  return td
    where cell = tape!!position

executeWrite :: TapeDevice -> IO TapeDevice
executeWrite (position, tape) = do
  ch <- getChar
  let newTape = take position tape ++ [ord ch] ++ drop (position+1) tape
  return (position, newTape)
