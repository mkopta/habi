module Habi.InputParser where

import Text.ParserCombinators.Parsec
import Control.Monad(liftM)
import Habi.Data

parseSource :: FilePath -> String -> Either ParseError Program
parseSource = parse program

program     = noNop >> blocks >>= \b -> eof >> return b
block       = instruction <|> blockCycle
blocks      = block `endBy` noNop
blockCycle  = liftM Cycle $ between (char '[') (char ']') (noNop >> blocks)
instruction = liftM Instruction (oneOf "><+-,.")
noNop       = many (noneOf "<>+-[],.")
