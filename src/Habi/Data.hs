module Habi.Data where

type Program = [Block]

data Block = Instruction Instruction | Cycle [Block]
  deriving (Show, Eq)

type Instruction = Char

type Cell = Int
type Tape = [Cell]
type TapePosition = Int
type TapeDevice = (TapePosition, Tape)
